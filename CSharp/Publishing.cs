namespace Program
{

    class Publishing
    {
        static Logger log = new Logger("Publishing");
        public static void Run()
        {
            var manager = AnnouncementManager.Instance;

            log.i("Создать объявление");
            manager.makeNewAnnouncement();

            log.i("Проставить параметры жилья");
            Decimal avgprice = manager.setDesc(
                new Description(
                    "Sample description",
                    60,
                    2
                )
            );

            log.i($"avg price = {avgprice}");
            Decimal myprice = avgprice - 500;

            log.i($"Проставить цену {myprice}");
            manager.setPrice(myprice);

            log.i($"Опубликовать");
            manager.publish();
        }
    }

    class Announcement
    {
        Logger log = new Logger("Announcement");
        public String title = "Not created";
        public Description description = new Description("",0,0);
        public Decimal price = 0;
        public String time = "Not created";
        public String owner = "Not owned";

        public override string ToString()
        {
            return
                $"Title: {title}\n" +
                $"Description:\n===\n{description}===\n" +
                $"Price: {price}\n" +
                $"Time: {time}\n" +
                $"Owner ID: {owner}\n";
        }

        public Announcement() {
            log.i("create()");
            title = "Created";
            time = "01-02-23";
            owner = "Owner";
        }

        public void setDesc(Description param)
        {
            log.i($"setDesc(\n{param})");
            description = param;
        }

        public void setPrice(Decimal price)
        {
            log.i($"setPrice({price})");
            this.price = price;
        }
    }

    class Description
    {
        public String text;
        public int area;
        public int rooms;

        public override string ToString()
        {
            return
                $"Text: {text}\n" +
                $"Area: {area}m2\n" +
                $"Rooms: {rooms}\n";
        }

        public Description(String text, int area, int rooms) {
            this.text = text;
            this.area = area;
            this.rooms = rooms;
        }
    }

    class Photo
    {
        public String owner;

        public override string ToString()
        {
            return
                $"Owner: {owner}\n";
        }

        public Photo(String owner) {
            this.owner = owner;
        }
    }

    class PublishServer
    {
        // Singleton vvv
        private PublishServer() {}
        static PublishServer() {}
        private static PublishServer _instance = new PublishServer();
        public static PublishServer Instance { get { return _instance; }}
        // Singleton ^^^

        Logger log = new Logger("Server");
        public void publish(Announcement announcement)
        {
            log.i($"publish(\n{announcement})");
        }
    }

    class AnnouncementManager
    {
        // Singleton vvv
        private AnnouncementManager() {}
        static AnnouncementManager() {}
        private static AnnouncementManager _instance = new AnnouncementManager();
        public static AnnouncementManager Instance { get { return _instance; }}
        // Singleton ^^^

        Logger log = new Logger("AnnouncementManager");

        Announcement current;

        public void makeNewAnnouncement()
        {
            log.i("makeNewAnnouncement()");

            current = new Announcement();
        }

        public Decimal setDesc(Description param)
        {
            log.i($"setDesc(\n{param})");

            current?.setDesc(param);
            return MarketAnalyzer.Instance.getAvgPrice(param);
        }

        public void setPrice(Decimal price)
        {
            log.i($"setPrice({price})");

            current?.setPrice(price);
        }

        public void publish()
        {
            log.i("publish()");

            PublishServer.Instance.publish(current);
        }
    }

    class MarketAnalyzer
    {
        // Singleton vvv
        private MarketAnalyzer() {}
        static MarketAnalyzer() {}
        private static MarketAnalyzer _instance = new MarketAnalyzer();
        public static MarketAnalyzer Instance { get { return _instance; }}
        // Singleton ^^^

        Logger log = new Logger("MarketAnalyzer");

        public Decimal getAvgPrice(Description param)
        {
            log.i($"getAvgPrice(\n{param})");

            return param.area * 1000 + param.rooms * 5000 + 10000;
        }

        // Для IssueRent.cs
        public void collect(Request req)
        {
            log.i("Collected request");
        }
    }
}