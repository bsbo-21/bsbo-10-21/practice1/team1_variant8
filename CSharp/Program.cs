﻿namespace Program {
    class Program {
        public static void Main(string[] args) {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Console.WriteLine("\n== Выставление жилья ==\n");
            Publishing.Run();

            Console.WriteLine("\n== Оформление аренды ==\n");
            IssueRent.Run();
        }
    }

    // ====================================

    class Logger
    {
        string title;

        public Logger(string title)
        {
            this.title = title;
        }

        // info
        public void i(string s) => Console.WriteLine($"[{title}] > {s}");
    }
}