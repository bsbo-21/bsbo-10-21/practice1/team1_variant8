namespace Program
{

    // главный класс прецедента, работает съемщика
    class IssueRent
    {
        static Logger log = new Logger("IssueRent");
        public static void Run()
        {
            var manager = RentManager.Instance;

            log.i("Создать запрос");
            manager.makeNewRequest();

            log.i("Проставить параметры жилья");
            manager.setParams("Description", 42);

            log.i("Отправить запрос");
            manager.send();
        }

        public static void ShowMessage(string msg)
        {
            log.i($"Получено сообщение - {msg}");
        }
    }

    class Renter
    {
        // Singleton vvv
        private Renter() {}
        static Renter() {}
        private static Renter _instance = new Renter();
        public static Renter Instance { get { return _instance; }}
        // Singleton ^^^
        static Logger log = new Logger("Renter");

        public void receiveInvoice(Decimal price)
        {
            log.i($"Получен счет - {price}");
            // оплачивает
            RentServer.Instance.confirmPayment();
        }

        public void ShowMessage(string msg)
        {
            log.i($"Получено сообщение - {msg}");
        }
    }

    class Request
    {
        Logger log = new Logger("Request");
        public string description = "";
        public Decimal price = 0;

        public override string ToString()
        {
            return
                $"Description: {description}\n" +
                $"Price: {price}\n";
        }

        public Request() {
            log.i("create()");
            description = "Created";
        }

        public void setParams(string desc, Decimal price)
        {
            log.i($"setDesc({desc},{price})");
            description = desc;
            this.price = price;
        }
    }

    class RentManager
    {
        // Singleton vvv
        private RentManager() {}
        static RentManager() {}
        private static RentManager _instance = new RentManager();
        public static RentManager Instance { get { return _instance; }}
        // Singleton ^^^

        Logger log = new Logger("RentManager");

        Request current;

        public void makeNewRequest()
        {
            log.i("makeNewRequest()");
            current = new Request();
        }

        public void setParams(string desc, Decimal price)
        {
            log.i($"setDesc({desc},{price})");
            current.setParams(desc, price);
        }

        public void send()
        {
            log.i("send()");
            RentServer.Instance.getInvoice(current);
        }

        public void proceed()
        {
            log.i("proceed()");
            IssueRent.ShowMessage("Оплачено");
            Renter.Instance.ShowMessage("Оплачено");

            // реализация в Publishing.cs
            MarketAnalyzer.Instance.collect(current);
        }
    }

    class RentServer
    {
        // Singleton vvv
        private RentServer() {}
        static RentServer() {}
        private static RentServer _instance = new RentServer();
        public static RentServer Instance { get { return _instance; }}
        // Singleton ^^^

        Logger log = new Logger("Server");
        public void getInvoice(Request request)
        {
            log.i($"getInvoice(\n{request})");
            Renter.Instance.receiveInvoice(request.price);
        }

        public void confirmPayment()
        {
            log.i($"confirmPayment()");
            // тут проверка платежа
            RentManager.Instance.proceed();
        }
    }
}